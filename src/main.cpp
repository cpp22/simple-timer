#include <iostream>
#include <vector>
#include "timer.hpp"

struct fat {
	long a{};
};

struct foo;

struct bar {
	const foo *owner;

	bar(foo *owner) : owner(owner) {}
};

struct foo {
	foo() {
		count += 1;
		name = "foo-" + std::to_string(count);
	}

	static uint32_t count;
	int x{0};
	int y{0};

	std::string name;

	std::vector<bar> bars;
};

uint32_t foo::count = 0;

int main() {
	std::cout << "\nHello, za Warudo!\n" << std::endl;

	auto foos = new std::vector<foo>();
	auto foosP = new std::vector<foo *>();
	auto fatP = new std::vector<fat *>();
	const uint32_t ammount = 10000000;//max ammount
//	const uint32_t ammount = 10;
	foos->reserve(ammount);
	foosP->reserve(ammount);
	fatP->reserve(ammount);

	p6t::timer timer;
	for (int i = 0; i < ammount; ++i) {
		foos->emplace_back(foo());
		fatP->emplace_back(new fat());
		foosP->emplace_back(new foo());
	}
	std::cout << "foos = new std::vector<foo>() and foosP = new std::vector<foo *>(): " << '\n';
	std::cout << "foos and foosP vectors cap: " << ammount << '\n';

	timer.reset();
	for (const auto &f : *foos) {
		int x = f.x;
		int y = f.y;
	}
	std::cout << "foos foreach access only elapsedTime: " << timer.elapsedTime() << " microseconds." << '\n';

	timer.reset();
	for (int i = 0; i < foos->size(); ++i) {
		(*foos)[i].x;
		(*foos)[i].y;
	}
	std::cout << "foos for access only elapsedTime: " << timer.elapsedTime() << " microseconds." << '\n';

	timer.reset();
	auto size = foos->size();
	for (int i = 0; i < size; ++i) {
		(*foos)[i].x;
		(*foos)[i].y;
	}
	std::cout << "foos for access only pre size aloc elapsedTime: " << timer.elapsedTime() << " microseconds." << '\n';

	timer.reset();
	auto size3 = foos->size();
	for (int i = 0; i < size3; ++i) {
		(*foos)[i].x = 2;
		(*foos)[i].y = 2;
	}
	std::cout << "foos for modify values pre size aloc elapsedTime: " << timer.elapsedTime() << " microseconds."
	          << '\n';


	timer.reset();
	for (const auto &f : *foosP) {
		int x = f->x;
		int y = f->y;
	}
	std::cout << "foosP foreach access only elapsedTime: " << timer.elapsedTime() << " microseconds." << '\n';

	timer.reset();
	for (int i = 0; i < foosP->size(); ++i) {
		(*foosP)[i]->x;
		(*foosP)[i]->y;
	}
	std::cout << "foosP for access only elapsedTime: " << timer.elapsedTime() << " microseconds." << '\n';

	timer.reset();
	auto size2 = foosP->size();
	for (int i = 0; i < size2; ++i) {
		(*foosP)[i]->x;
		(*foosP)[i]->y;
	}
	std::cout << "foosP for access only pre aloc size elapsedTime: " << timer.elapsedTime() << " microseconds." << '\n';

	timer.reset();
	auto size4 = foosP->size();
	for (int i = 0; i < size4; ++i) {
		(*foosP)[i]->x = 2;
		(*foosP)[i]->y = 2;
	}
	std::cout << "foosP for modify values pre aloc size elapsedTime: " << timer.elapsedTime() << " microseconds."
	          << '\n';

	timer.reset();
	foos->emplace_back(foo());
	std::cout << "foos emplace overflow: " << timer.elapsedTime() << " microseconds." << '\n';

	timer.reset();
	foosP->emplace_back(new foo());
	std::cout << "foosP emplace overflow: " << timer << '\n';

	timer.reset();
	delete foos;
	std::cout << "Delete foos " << timer << '\n';

	auto fatDelete = foosP->size();
	for (int i = 0; i < fatDelete; ++i) {
		delete (*fatP)[i];
	}
	delete fatP;

	timer.reset();
	auto toDelete = foosP->size();
	for (int i = 0; i < toDelete; ++i) {
		delete (*foosP)[i];
	}
	delete foosP;
	std::cout << "Delete foosP " << timer << '\n';


	std::cout << '\n' << "To be Continued..." << std::endl;
	return 0;
}