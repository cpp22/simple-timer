//
// Created by (P6) - Pedro Augusto de Almeida Soares on 14/07/2021.
//

#ifndef _TIMER_P6_TIMER_
#define _TIMER_P6_TIMER_

#include <chrono>
#include <atomic>
#include <string>

namespace p6t {
	using nanoseconds = std::chrono::nanoseconds;
	using microseconds = std::chrono::microseconds;
	using milliseconds = std::chrono::milliseconds;

	class timer {
		using clock = std::chrono::high_resolution_clock;
		typename clock::time_point startPoint;

	public:
		//simple constructor
		timer() : startPoint(clock::now()) {}

		//Default unit type is in microseconds
		template<typename units = microseconds>
		[[nodiscard]] double elapsedTime() const {
			std::atomic_thread_fence(std::memory_order_relaxed);
			auto countedTime = std::chrono::duration_cast<units>(clock::now() - startPoint).count();
			std::atomic_thread_fence(std::memory_order_relaxed);
			return static_cast<double>(countedTime);
		}

		void reset() {
			startPoint = clock::now();
		}

		//couts timer elapsed time in microseconds
		friend std::ostream &operator<<(std::ostream &os, const timer &timer) {
			os << "Elapsed Time: " << timer.elapsedTime() << " microseconds.";
			return os;
		}
	};

}
#endif //_TIMER_P6_TIMER_
